##Profesionalūs anglų kalbos kursai Vilniuje ir Kaune

Esame maksimalistai, kurių pagrindinis tikslas – nauda Jums. Kartu su Jumis užsibrėžiame tikslus ir dedame visas pastangas, kad juos pasiektumėte. Išmoksite naujų kalbų, įgysite naujų įgūdžių – Jūsų sėkmė yra didžiausias mūsų džiaugsmas ir mes neturime ribų.
Mokydamiesi, augdami ir tobulėdami, atversite duris naujoms mokslo, karjeros bei socialinio gyvenimo galimybėms ir jose pasieksite daugiau. Mokymo centre „iCAN“ pasirengsite visiems akademiniams ir darbiniams iššūkiams – nesvarbu, koks Jūsų amžius ir kur gyvenate. Turime reikiamą patirtį ir reikiamus įrankius. „iCAN“ mokymo centras sukurtas Jums ir Jūsų sėkmei.
Tikime, kad visada yra ko išmokti ir kur tobulėti.  Kad ir kur dirbtumėte, ko siektumėte, esame šventai įsitikinę, kad esate verti daugiau. Žinios, patirtis ir naujos kompetencijos padės Jums greičiau tai pasiekti.
Atminkite, kad žinių niekada nebus per daug!

iCAN mokymo centre išskirtinis dėmesys kiekvienam, aktualios mokymo programos bei kvalifikuoti lektoriai.
Po kursų tu:

* Lengvai ir užtikrintai bendrausi anglų kalba atostogaudamas ar dirbdamas užsienyje.
* Tapsi kvalifikuotu, konkurencingu ir kompetetingu darbuotoju tarptautinėse kompanijose.
* Profesionalių dėstytojų pagalba įgysi žinių, kurios pravers bet kurioje situacijoje visam gyvenimui!

Štai 5 taisyklės mokantis anglų kalbos:

1. Niekada negalvok, jog mokytis bus lengva ir greita. 
2. Pagrindinis ir svarbiausias kalbos mokytojas – esi tu pats. 
3. Mesk iš galvos mintis “Metus pailsėsiu, vėliau sugrįšiu”.
4. Nebijok investuoti į žinias. Tai amžina ir visada atsiperkanti investicija.
5. Domėkis ir ieškok informacijos apie patikrintus, patikimus ir profesionalius mokymo metodus.

Registracija ir informacija: +370 670 44317; registracija@ican.lt;

Mes padėsime Tau pasiekti aukščiausių rezultatų!
Savanorių pr. 273, Kaune
Savanorių 63, Vilniuje

[http://www.ican.lt/kursai/anglu-kalbos-kursai/](http://www.ican.lt/kursai/anglu-kalbos-kursai/)